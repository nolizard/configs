#
# bashrc - Martin Pieuchot
#  http://www.grenadille.net
#

if [ -f "/etc/profile" ] ; then
	source /etc/profile
fi

if [ -x /usr/bin/dircolors ] ; then
	# If dircolors(1) is present it's likely a GNU ls
	alias ls="ls --color=auto -F"

	if [ -f "$HOME/.dircolors" ] ; then
		eval `/usr/bin/dircolors -b $HOME/.dircolors`
	fi
fi

alias ls='ls --color=auto -F'
alias ll='ls -l'
alias la='ls -la'
