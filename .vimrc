"
" vimrc - Martin Pieuchot
"  http://www.grenadille.net
"

syntax on
colorscheme peachpuff

set encoding=utf-8
set fileencoding=utf-8

set nocompatible	" (cp) prevents from original vi's bugs & limitations
set ruler 		" (ru) display the curent cursor position
set showcmd		" (sc) display incomplete commands
set nonumber		" (nu) don't show line numbers
set showmatch		" (sm) verify brace/parenthes/bracket
set autoindent		" (ai) auto indentation
set copyindent		" (ci) use the indenting format of the previous line
"set smartindent	" (sta) 'sw' in front of a line, 'ts' otherwise

"set textwidth=78	" (tw) number of columns before auto line break.
set laststatus=1	" (ls) mixed status line
set backspace=2		" (bs) backspace of items not from the current session
set scrolloff=3		" (so) keep 3 lines when scrolling
set visualbell t_vb=	" (vb) disable horrible beeps

set hlsearch		" (hls) highlight searches
set incsearch		" (is) search letter by letter
set ignorecase		" (ic) ignore case when searching
set smartcase		" (scs) don't ignore case when we use uppercases
set infercase		" (inf) fix case of new word during completion

set nobackup		" (bk) do not keep a backup file
set noswapfile		" (swf) do not create a swap file


au BufEnter *.h :setlocal filetype=c

" (sts) makes spaces feel like tabs (like deleting)
au FileType c setlocal cindent
au FileType python setlocal ts=4 sw=4 sts=4 et
au FileType xhtml,html,htm,php,xml setlocal ts=2 sw=2 sts=2

au FileType c,python match ErrorMsg '\%>80v.\+'
au FileType c,python,diff match ErrorMsg '\s\+$'

" Keyboard mappings
"

" ctr+t : newtab, ctr+a/z lefttab & righttab
map <C-t> :tabnew<CR>
map <F3> :tabprevious<CR>
map <F4> :tabnext<CR>


" Autocomplete
iab #i #include
iab #d #define


" Removes trailing whitespaces
function! RemoveWhiteSpace()
  :%s/\s\+$//e
endfunction

