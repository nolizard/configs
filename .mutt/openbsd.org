# openbsd.org account

set from		= "Martin Pieuchot <mpi@>"

set status_format	= "-- OpenBSD: %f [Msgs:%M %l]---(%s/%S)%|-"

alternates		"mpi(euchot)?@"

unmailboxes	*
mailboxes	`for i in $(ls $HOME/.mail/) ; do echo -n " =$i" ; done`

# Mailing lists I'm subscribed to.
subscribe source-changes@openbsd.org
subscribe ports-changes@openbsd.org
subscribe bugs@openbsd.org
subscribe tech@openbsd.org
subscribe hackers@openbsd.org
subscribe gameoftrees@openbsd.org

# Circle through accounts
macro index S "<enter-command>source ~/.mutt/grenadille.net<enter>"

