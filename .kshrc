#
# kshrc - Martin Pieuchot
#  http://www.grenadille.net
#

if [ -f "/etc/ksh.kshrc" ] ; then
	. /etc/ksh.kshrc
fi

if [ -x /usr/local/bin/colorls ] ; then
	alias ls="colorls -GF"
elif [ -x /usr/bin/dircolors ] ; then
	# If dircolors(1) is present it's likely a GNU ls
	alias ls="ls --color=auto -F"

	if [ -f "$HOME/.dircolors" ] ; then
		eval `/usr/bin/dircolors -b $HOME/.dircolors`
	fi
fi

stty status ^T

alias ll="ls -l"
alias la="ls -la"
alias cvsync="reposync rsync://ftp.hostserver.de/cvsync/"
alias pman='man -M /usr/local/share/doc/posix/man'

export PS1="$USER@$HOST $ "
export HISTFILE="$HOME/.history"
export GPG_TTY=$(tty)
export TOG_COLORS=
