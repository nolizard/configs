#
# profile - Martin Pieuchot
#  http://www.grenadille.net
#

ENV=$HOME/.kshrc

PATH=$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin:/usr/games:/usr/ports/infrastructure/bin:.

PAGER="/usr/bin/less -i"

LC_CTYPE="en_US.UTF-8"

EDITOR="vim"

export PATH HOME ENV PAGER LC_CTYPE EDITOR
